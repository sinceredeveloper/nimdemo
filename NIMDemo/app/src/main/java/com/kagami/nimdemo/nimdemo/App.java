package com.kagami.nimdemo.nimdemo;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.activeandroid.ActiveAndroid;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.SDKOptions;
import com.netease.nimlib.sdk.StatusBarNotificationConfig;
import com.netease.nimlib.sdk.auth.LoginInfo;

/**
 * Created by sinceredeveloper on 16/1/20.
 */
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        if (inMainProcess(this)){
            ActiveAndroid.initialize(this);
        }
        NIMClient.init(this, loginInfo(), options());
    }

    private SDKOptions options() {
        SDKOptions options = new SDKOptions();

        // 如果将新消息通知提醒托管给 SDK 完成，需要添加以下配置。否则无需设置。
        StatusBarNotificationConfig config = new StatusBarNotificationConfig();
        config.notificationEntrance = Main2Activity.class; // 点击通知栏跳转到该Activity
        config.notificationSmallIconId = R.drawable.ic_menu_send;
        options.statusBarNotificationConfig = config;
        return options;
    }
    public LoginInfo loginInfo(){
        SharedPreferences sp=getSharedPreferences("info",Context.MODE_PRIVATE);
        String account=sp.getString("account", "");
        String token=sp.getString("token","");
        if(account.length()>0 && token.length()>0){
            return new LoginInfo(account,token);
        }
        return null;
    }
    public static boolean inMainProcess(Context context) {
        String packageName = context.getPackageName();
        String processName = getProcessName(context);
        return packageName.equals(processName);
    }

    /**
     * 获取当前进程名
     * @param context
     * @return 进程名
     */
    public static final String getProcessName(Context context) {
        String processName = null;

        // ActivityManager
        ActivityManager am = ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE));

        while (true) {
            for (ActivityManager.RunningAppProcessInfo info : am.getRunningAppProcesses()) {
                if (info.pid == android.os.Process.myPid()) {
                    processName = info.processName;
                    break;
                }
            }

            // go home
            if (!TextUtils.isEmpty(processName)) {
                return processName;
            }

            // take a rest and again
            try {
                Thread.sleep(100L);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
