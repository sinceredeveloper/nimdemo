package com.kagami.nimdemo.nimdemo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.StatusCode;
import com.netease.nimlib.sdk.auth.AuthService;
import com.netease.nimlib.sdk.auth.AuthServiceObserver;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.msg.model.RecentContact;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Observer<List<IMMessage>> incomingMessageObserver;
    List<RecentContact> dataSet;
    @Bind(R.id.listView)
    ListView listView;

    RecentContactAdapter adapter;
    Observer<List<RecentContact>> recentContactObserver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Log.d("shino", "Main2Activity onCreate");
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        dataSet=new ArrayList<RecentContact>();
        adapter=new RecentContactAdapter();
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i=new Intent(Main2Activity.this,MessageActivity.class);
                i.putExtra("data",dataSet.get(position));
                startActivity(i);
            }
        });
        refresh();
        //  创建观察者对象
        recentContactObserver =
                new Observer<List<RecentContact>>() {
                    @Override
                    public void onEvent(List<RecentContact> messages) {
                        refresh();
                    }
                };
//  注册/注销观察者
        NIMClient.getService(MsgServiceObserve.class)
                .observeRecentContact(recentContactObserver, true);

//        incomingMessageObserver =
//                new Observer<List<IMMessage>>() {
//                    @Override
//                    public void onEvent(List<IMMessage> messages) {
//                        // 处理新收到的消息，为了上传处理方便，SDK 保证参数 messages 全部来自同一个聊天对象。
//                        for(IMMessage item:messages){
//                            Log.d("message", item.getContent());
//
//                        }
//                    }
//                };
//        NIMClient.getService(MsgServiceObserve.class)
//                .observeReceiveMessage(incomingMessageObserver, true);

        LoginInfo linfo=((App)getApplication()).loginInfo();
        if(linfo==null){
            Intent i=new Intent(this,LoginActivity.class);
            startActivity(i);
        }else{

            TextView tv=(TextView)navigationView.getHeaderView(0).findViewById(R.id.header_account);
            tv.setText(linfo.getAccount());
        }
        NIMClient.getService(AuthServiceObserver.class).observeOnlineStatus(
                new Observer<StatusCode> () {
                    public void onEvent(StatusCode status) {
                        Snackbar.make(fab, "登录状态改变:"+status, Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }, true);
    }

    void refresh() {
        NIMClient.getService(MsgService.class).queryRecentContacts()
                .setCallback(new RequestCallbackWrapper<List<RecentContact>>() {
                    @Override
                    public void onResult(int code, List<RecentContact> recents, Throwable e) {
                        if (recents != null && recents.size() > 0) {
                            dataSet = recents;
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        NIMClient.getService(MsgService.class).setChattingAccount(MsgService.MSG_CHATTING_ACCOUNT_ALL, SessionTypeEnum.None);
    }

    @Override
    protected void onPause() {
        super.onPause();
        NIMClient.getService(MsgService.class).setChattingAccount(MsgService.MSG_CHATTING_ACCOUNT_NONE, SessionTypeEnum.None);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NIMClient.getService(MsgServiceObserve.class)
                .observeRecentContact(recentContactObserver, false);
//        NIMClient.getService(MsgServiceObserve.class)
//                .observeReceiveMessage(incomingMessageObserver, false);
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            moveTaskToBack(true);
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main2, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if(id==R.id.nav_logout){
            NIMClient.getService(AuthService.class).logout();
            SharedPreferences.Editor ed=getSharedPreferences("info", Context.MODE_PRIVATE).edit();
            ed.putString("account","");
            ed.putString("token","");
            ed.commit();
            finish();
        }else if(id==R.id.nav_group){
            Intent i=new Intent(this,ContactsSelectActivity.class);
            startActivity(i);
        }
//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    class RecentContactAdapter extends BaseAdapter {
        SimpleDateFormat format;
        public RecentContactAdapter(){
            super();
            format=new SimpleDateFormat("yy/MM/dd HH:mm:ss");
        }
        @Override
        public int getCount() {
            return dataSet.size();
        }

        @Override
        public Object getItem(int position) {
            return dataSet.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView != null) {
                holder = (ViewHolder) convertView.getTag();
            } else {
                convertView = LayoutInflater.from(Main2Activity.this).inflate(R.layout.item_recentcontact, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }
            RecentContact data=dataSet.get(position);
            if(data.getSessionType() == SessionTypeEnum.Team){
                holder.from.setText("群组ID:" + data.getContactId()+"   "+format.format(new Date(data.getTime())));
            }else {
                holder.from.setText(data.getContactId()+"   "+format.format(new Date(data.getTime())));
            }
            holder.context.setText(data.getFromAccount()+":"+data.getContent());
            return convertView;
        }



    }
    class ViewHolder {
        @Bind(R.id.from)
        TextView from;
        @Bind(R.id.context)
        TextView context;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
