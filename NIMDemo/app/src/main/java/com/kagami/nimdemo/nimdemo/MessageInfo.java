package com.kagami.nimdemo.nimdemo;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by sinceredeveloper on 16/1/20.
 */
@Table(name = "messages")
public class MessageInfo extends Model {
    @Column(name = "kfrom")
    public String from;
    @Column(name = "ktext")
    public String text;
    @Column(name = "ksid")
    public String sessionId;
    public MessageInfo(String from,String text,String sessionId){
        this.from=from;
        this.text=text;
        this.sessionId=sessionId;
    }

    static public List<MessageInfo> findBySession(String sessionId){
        return new Select()
                .from(MessageInfo.class)
                .where("ksid = ?",sessionId)
                .execute();
    }
}
