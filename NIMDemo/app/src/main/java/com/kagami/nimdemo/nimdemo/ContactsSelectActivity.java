package com.kagami.nimdemo.nimdemo;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.team.TeamService;
import com.netease.nimlib.sdk.team.constant.TeamFieldEnum;
import com.netease.nimlib.sdk.team.constant.TeamTypeEnum;
import com.netease.nimlib.sdk.team.model.Team;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ContactsSelectActivity extends AppCompatActivity {

    List<KContact> dataSet;
    ListView listView;
    ContactAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_select);
        listView = (ListView) findViewById(R.id.listView);
        dataSet = new ArrayList<>();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                // 群组类型
                TeamTypeEnum type = TeamTypeEnum.Normal;
                List<String> member=new ArrayList<>();
                for(KContact item : dataSet){
                    if (item.isSelect){
                        member.add(item.account);
                    }
                }
                if (member.size()<2){
                    Snackbar.make(view, "至少选择两个人啊", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    return;
                }
                HashMap<TeamFieldEnum, Serializable> fields = new HashMap<TeamFieldEnum, Serializable>();
                fields.put(TeamFieldEnum.Name, "team");
                NIMClient.getService(TeamService.class).createTeam(fields, type, "", member).setCallback(new RequestCallback<Team>() {
                    @Override
                    public void onSuccess(Team team) {
                        finish();
                    }

                    @Override
                    public void onFailed(int i) {
                        Snackbar.make(view, "失败", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        Snackbar.make(view, "失败", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                });
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dataSet.add(new KContact("test1"));
        dataSet.add(new KContact("test2"));
        dataSet.add(new KContact("test3"));
        dataSet.add(new KContact("test4"));
        dataSet.add(new KContact("test5"));
        dataSet.add(new KContact("test6"));
        dataSet.add(new KContact("test7"));
        dataSet.add(new KContact("test8"));
        adapter = new ContactAdapter();
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                KContact data = dataSet.get(position);
                data.isSelect = !data.isSelect;
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Snackbar.make(listView, "点击右下角按钮创建群组", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }



    class ContactAdapter extends BaseAdapter {
        public ContactAdapter() {
            super();
        }

        @Override
        public int getCount() {
            return dataSet.size();
        }

        @Override
        public Object getItem(int position) {
            return dataSet.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(ContactsSelectActivity.this).inflate(R.layout.item_contact, parent, false);
            }
            KContact data = dataSet.get(position);
            //((TextView)convertView.findViewById(R.id.textView)).setText(data.account);
            if (data.isSelect) {
                ((TextView) convertView.findViewById(R.id.textView)).setText(data.account + "(选中)");
            } else {
                ((TextView) convertView.findViewById(R.id.textView)).setText(data.account);
            }
            return convertView;
        }


    }

    class KContact {
        String account;
        boolean isSelect = false;

        public KContact(String account) {
            this.account = account;
        }
    }

}
