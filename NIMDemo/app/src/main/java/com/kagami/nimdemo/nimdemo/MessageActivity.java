package com.kagami.nimdemo.nimdemo;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.model.CustomMessageConfig;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.msg.model.QueryDirectionEnum;
import com.netease.nimlib.sdk.msg.model.RecentContact;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MessageActivity extends AppCompatActivity {

    RecentContact data;
    List<IMMessage> dataSet;
    ListView listView;
    MessageAdapter adapter;
    Observer<List<IMMessage>>  incomingMessageObserver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        listView=(ListView)findViewById(R.id.listView);
        data=(RecentContact)getIntent().getSerializableExtra("data");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(data.getContactId());
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final View v = LayoutInflater.from(MessageActivity.this).inflate(R.layout.dialog_send, null);
                 AlertDialog alertdialog=new AlertDialog.Builder(MessageActivity.this)
                        .setTitle("发送")
                        .setPositiveButton("发送",new Dialog.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                EditText et=(EditText)v.findViewById(R.id.editText);
                                CheckBox cb=(CheckBox)v.findViewById(R.id.checkBox);
                                String text=et.getText().toString();
                                if (text.length()<1){
                                    return;
                                }
                                final IMMessage message = MessageBuilder.createTextMessage(
                                data.getContactId(), // 聊天对象的 ID，如果是单聊，为用户帐号，如果是群聊，为群组 ID
                                data.getSessionType(), // 聊天类型，单聊或群组
                                 text // 文本内容
                                );
                                if(cb.isChecked()){
                                    CustomMessageConfig cc=new CustomMessageConfig();
                                    cc.enablePush=false;
                                    message.setConfig(cc);
                                }
                                NIMClient.getService(MsgService.class).sendMessage(message, true).setCallback(new RequestCallback<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        dataSet.add(0,message);
                                        adapter.notifyDataSetChanged();
                                    }

                                    @Override
                                    public void onFailed(int i) {

                                    }

                                    @Override
                                    public void onException(Throwable throwable) {

                                    }
                                });

                            }
                        })
                        .setNegativeButton("取消",new Dialog.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setView(v)
                        .setCancelable(true).create();
                alertdialog.show();
                return;
//                // 创建文本消息
//                IMMessage message = MessageBuilder.createTextMessage(
//                        data.getContactId(), // 聊天对象的 ID，如果是单聊，为用户帐号，如果是群聊，为群组 ID
//                        data.getSessionType(), // 聊天类型，单聊或群组
//                        "ceshi" // 文本内容
//                );
//// 发送消息。如果需要关心发送结果，可设置回调函数。发送完成时，会收到回调。如果失败，会有具体的错误码。
//                NIMClient.getService(MsgService.class).sendMessage(message, true);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        dataSet=new ArrayList<>();
        adapter=new MessageAdapter();
        listView.setAdapter(adapter);


        IMMessage msg=MessageBuilder.createEmptyMessage(data.getContactId(),data.getSessionType(),0);
        NIMClient.getService(MsgService.class).queryMessageListEx(msg, QueryDirectionEnum.QUERY_NEW,100,false).setCallback(new RequestCallbackWrapper<List<IMMessage>>() {
            @Override
            public void onResult(int i, List<IMMessage> imMessages, Throwable throwable) {
                dataSet = imMessages;
                adapter.notifyDataSetChanged();
            }
        });

         incomingMessageObserver =
                new Observer<List<IMMessage>>() {
                    @Override
                    public void onEvent(List<IMMessage> messages) {
                        // 处理新收到的消息，为了上传处理方便，SDK 保证参数 messages 全部来自同一个聊天对象。
                        boolean needReload=false;
                        for(IMMessage msg : messages) {
                            //Log.d("messageactivity",)
                            if(msg.getSessionType()==data.getSessionType() && msg.getSessionId().equals(data.getContactId())){
                                dataSet.add(0,msg);
                                needReload=true;
                            }
                        }
                        if(needReload){
                            adapter.notifyDataSetChanged();
                        }
                    }
                };
        NIMClient.getService(MsgServiceObserve.class)
                .observeReceiveMessage(incomingMessageObserver, true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        NIMClient.getService(MsgService.class).setChattingAccount(data.getContactId(),data.getSessionType());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NIMClient.getService(MsgServiceObserve.class)
                .observeReceiveMessage(incomingMessageObserver, false);
    }

    class MessageAdapter extends BaseAdapter {
        SimpleDateFormat format;
        public MessageAdapter(){
            super();
            format=new SimpleDateFormat("yy/MM/dd HH:mm:ss");
        }
        @Override
        public int getCount() {
            return dataSet.size();
        }

        @Override
        public Object getItem(int position) {
            return dataSet.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView != null) {
                holder = (ViewHolder) convertView.getTag();
            } else {
                convertView = LayoutInflater.from(MessageActivity.this).inflate(R.layout.item_message, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }
            IMMessage data=dataSet.get(position);
            holder.from.setText( data.getFromAccount()+"   "+format.format(new Date(data.getTime())));
            holder.context.setText(data.getContent());
            return convertView;
        }



    }
    class ViewHolder {
        @Bind(R.id.from)
        TextView from;
        @Bind(R.id.context)
        TextView context;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
