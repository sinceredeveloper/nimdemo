package com.kagami.nimdemo.nimdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.msg.model.RecentContact;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    Observer<List<IMMessage>> incomingMessageObserver;
    List<RecentContact> dataSet;
    @Bind(R.id.listView)
    ListView listView;
    RecentContactAdapter adapter;
    Observer<List<RecentContact>> recentContactObserver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dataSet=new ArrayList<RecentContact>();
        adapter=new RecentContactAdapter();
        listView.setAdapter(adapter);

        NIMClient.getService(MsgService.class).queryRecentContacts()
                .setCallback(new RequestCallbackWrapper<List<RecentContact>>() {
                    @Override
                    public void onResult(int code, List<RecentContact> recents, Throwable e) {
                        if (recents!=null && recents.size()>0){
                            dataSet=recents;
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
        //  创建观察者对象
        recentContactObserver =
                new Observer<List<RecentContact>>() {
                    @Override
                    public void onEvent(List<RecentContact> messages) {
                        dataSet=messages;
                        adapter.notifyDataSetChanged();
                    }
                };
//  注册/注销观察者
        NIMClient.getService(MsgServiceObserve.class)
                .observeRecentContact(recentContactObserver, true);

//        incomingMessageObserver =
//                new Observer<List<IMMessage>>() {
//                    @Override
//                    public void onEvent(List<IMMessage> messages) {
//                        // 处理新收到的消息，为了上传处理方便，SDK 保证参数 messages 全部来自同一个聊天对象。
//                        for(IMMessage item:messages){
//                            Log.d("message", item.getContent());
//
//                        }
//                    }
//                };
//        NIMClient.getService(MsgServiceObserve.class)
//                .observeReceiveMessage(incomingMessageObserver, true);

        LoginInfo linfo=((App)getApplication()).loginInfo();
        if(linfo==null){
            Intent i=new Intent(this,LoginActivity.class);
            startActivity(i);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NIMClient.getService(MsgServiceObserve.class)
                .observeRecentContact(recentContactObserver, false);
//        NIMClient.getService(MsgServiceObserve.class)
//                .observeReceiveMessage(incomingMessageObserver, false);
    }


    class RecentContactAdapter extends BaseAdapter{
        @Override
        public int getCount() {
            return dataSet.size();
        }

        @Override
        public Object getItem(int position) {
            return dataSet.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView != null) {
                holder = (ViewHolder) convertView.getTag();
            } else {
                convertView = LayoutInflater.from(MainActivity.this).inflate(R.layout.item_recentcontact, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }
            RecentContact data=dataSet.get(position);
            holder.from.setText(data.getFromAccount());
            holder.context.setText(data.getContent());
            return convertView;
        }



    }
    class ViewHolder {
        @Bind(R.id.from)
        TextView from;
        @Bind(R.id.context)
        TextView context;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
