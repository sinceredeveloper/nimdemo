package com.kagami.nimdemo.nimdemo;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by sinceredeveloper on 16/1/20.
 */
@Table(name = "sessions")
public class MessageSession  extends Model {
    @Column(name = "kfrom")
    public String from;
    @Column(name = "ktext")
    public String text;
    @Column(name = "ksid")
    public String sessionId;
    public MessageSession(String from,String text,String sid){
        this.from=from;
        this.text=text;
        this.sessionId=sid;
    }

    static public List<MessageSession> findBySession(String sessionId){
        return new Select()
                .from(MessageSession.class)
                .where("ksid = ?",sessionId)
                .execute();
    }
}
